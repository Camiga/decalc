﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeCalc
{
    public partial class Form1 : Form
    {
        // Determine whether to reset screen to zero when user inputs.
        private bool operatorPressed = false;
        // Determine whether CE button clears everything.
        private bool equalsPressed = false;
        // Concatenate all operations here and evaluate in table.
        private string savedEquation = "";
        // Store last operation for repeating the equals button.
        private string lastOperation = "";
        // Store all operators in current equation for live calculating without pushing equals.
        private int operatorsTotal = 0;

        // Create two lists containing all buttons in the form and their respective colors.
        // This is used for handling theme toggling later without hardcoding their color values.
        private readonly List<Button> allButtons = new List<Button> { };
        private readonly List<Color> buttonColors = new List<Color> { };

        public Form1()
        {
            // result = Large box with answer and current number being typed.
            // equation = Small box above result in grey text with full equation.
            InitializeComponent();

            // Add all relevant buttons to the list above.
            allButtons.AddRange (new List<Button>
            {
                button1, button2, button3, button4, button5,
                button6, button7, button8, button9, button0,
                buttonPoint, buttonPlus, buttonMinus, buttonTimes, buttonDivide,
                buttonBack, buttonC, buttonCE, buttonNegative, buttonEquals
            });
            // Get the color of each button at runtime, and save it in the second list.
            allButtons.ForEach(selected => buttonColors.Add(selected.BackColor));
        }

        // Compute string equations and return double with no trailing .0's.
        private double Evaluate(string expression)
        {
            DataTable table = new DataTable();

            // If entry string is int32, replace with double by adding .0 to prevent out of range crash.
            if (expression.Contains("."))
                table.Columns.Add("expression", typeof(string), expression);
            else
                table.Columns.Add("expression", typeof(string), expression + ".0");

            DataRow row = table.NewRow();
            table.Rows.Add(row);
            return double.Parse((string)row["expression"]);
        }

        // Separate void for error handling any user evaluations. Prevents crashes e.g. divide by zero.
        private void RunCalculation(string input)
        {
            try
            {
                result.Text = Evaluate(input).ToString();
            } catch (Exception)
            {
                result.Text = "Error!";
            }
        }

        // Reset UI and relevant variables.
        private void ClearCalculator()
        {
            result.Text = "0";
            equation.Text = "";

            equalsPressed = false;
            operatorPressed = false;
            operatorsTotal = 0;
            savedEquation = "";
        }

        private void Number_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            // Clear memory if user wants to type new equation.
            if (equalsPressed)
                ClearCalculator();
            // Replace zero when not needed.
            if (result.Text == "0" || operatorPressed)
                result.Text = "";

            // Disable operation press on number press.
            operatorPressed = false;

            if (b.Text == ".")
            {
                // Prevent multiple decimal places from being entered.
                if (result.Text.Contains("."))
                    return;
                // Add zero to empty values.
                if (result.Text == "")
                {
                    result.Text = "0.";
                    return;
                }
            }

            // Add number to result text normally.
            result.Text += b.Text;
        }

        private void ButtonC_Click(object sender, EventArgs e)
        {
            ClearCalculator();
        }

        private void ButtonCE_Click(object sender, EventArgs e)
        {
            // Clear everything if the equals button has been pressed. Otherwise just reset current entry in result box.
            if (equalsPressed)
                ClearCalculator();
            else
                result.Text = "0";
        }

        private void Operator_Click(object sender, EventArgs e)
        {
            Button o = (Button)sender;
            // Set operationPressed to clear calculator digits on number press.
            operatorPressed = true;

            // Allow for using result at start of next calculation by pushing an operator.
            if (equalsPressed)
            {
                savedEquation = result.Text + o.Text;
                equation.Text = result.Text + $" {o.Text} ";
                equalsPressed = false;
            }
            else
            {
                // Use operator symbol normally.
                savedEquation += result.Text + o.Text;
                equation.Text += result.Text + $" {o.Text} ";
            }

            // Overwrite the last operation incase user hits equals more than once.
            lastOperation = $" {o.Text} ";

            // Keep track of times operator has been pressed.
            operatorsTotal++;
            // Cut excess operator off the end for running calculations each time operator button is pressed instead of equals.
            if (operatorsTotal >= 2)
                RunCalculation(savedEquation.Substring(0, savedEquation.Length - 1));
        }

        private void ButtonEquals_Click(object sender, EventArgs e)
        {
            // Prevent equals button from working if nothing to calculate.
            if (operatorsTotal <= 0)
                return;

            if (equalsPressed)
            {
                // If equals has been pressed twice, append the previous operation to the result number and recompute.
                equation.Text = result.Text + lastOperation + " = ";
                savedEquation = result.Text + lastOperation;
                RunCalculation(savedEquation);
            }
            else
            {
                // If equals is pressed for the first time, finish the lastOperation and run the equation as normal.
                lastOperation += result.Text;
                equation.Text += result.Text + " = ";
                equalsPressed = true;
                RunCalculation(savedEquation + result.Text);
            }
        }

        private void ButtonNegative_Click(object sender, EventArgs e)
        {
            if (result.Text.StartsWith("-"))
                // Remove - from number if it exists.
                result.Text = result.Text.Substring(1);
            else
                // Add - to number without space.
                result.Text = "-" + result.Text;
        }

        private void ButtonBg_Click(object sender, EventArgs e)
        {
            if (bgDialogue.ShowDialog() == DialogResult.OK)
                this.BackColor = bgDialogue.Color;
        }

        private void ButtonBack_Click(object sender, EventArgs e)
        {
            if (result.Text.Length <= 1 || equalsPressed || result.Text.Substring(0, result.Text.Length - 1) == "-")
                ClearCalculator();
            else
                result.Text = result.Text.Substring(0, result.Text.Length - 1);
        }

        private void CheckInvert_CheckedChanged(object sender, EventArgs e)
        {
            if (checkInvert.Checked)
            {
                // On positive check, set each button text color to white.
                allButtons.ForEach(bText => bText.ForeColor = Color.White);
                // Also update non-button controls, ensuring that the equation label is a variant of gray.
                result.ForeColor = Color.White;
                equation.ForeColor = Color.LightGray;
                checkInvert.ForeColor = Color.White;
                checkThemed.ForeColor = Color.White;
            }
            else
            {
                // On negative check, set color and variables to black.
                allButtons.ForEach(bText => bText.ForeColor = Color.Black);

                result.ForeColor = Color.Black;
                equation.ForeColor = Color.DimGray;
                checkInvert.ForeColor = Color.Black;
                checkThemed.ForeColor = Color.Black;
            }
        }

        private void CheckThemed_CheckedChanged(object sender, EventArgs e)
        {
            if (checkThemed.Checked)
            {
                // On positive check, set backgrond color of each button to their values stored at startup.
                for (int i = 0; i < allButtons.Count; i++)
                    allButtons[i].BackColor = buttonColors[i];
            }
            else
            {
                // On negative check, set background to transparent, without removing the stored colors in List<Color> buttonColors
                for (int i = 0; i < allButtons.Count; i++)
                    allButtons[i].BackColor = Color.Transparent;
            }
        }
    }
}
